var mongo = require("mongodb");
var express = require("express");

var graphqlTools = require("graphql-tools");
var graphqlClient = require("graphql");

var fs = require("fs");
//rest for file upload

var _ = require("underscore");

const mma_score_standings = require("../../phpdraft_code/mma_score_standings");
const mmaScoreStandingsHold = mma_score_standings();

var cors = require("cors");

//connect to mongodb this way
//mongo mongodb://localhost/draftbrackets to connect command line
const MONGO_URL = "mongodb://localhost/draftbrackets";

const prepare = o => {
  o._id = o._id.toString();
  return o;
};

export const start = async () => {
  process
    .on("unhandledRejection", (reason, p) => {
      console.error(reason, "Unhandled Rejection at Promise", p);
    })
    .on("uncaughtException", err => {
      console.error(err, "Uncaught Exception thrown");
      process.exit(1);
    });
  try {
    const db = await mongo.connect(MONGO_URL);

    const MmaContests = db.collection("mmacontestcollection");

    const UserBrackets = db.collection("userbracketscollection");
    const MmaPredictions = db.collection("mmapredictioncollection");
    const Fighters = db.collection("fighterscollection");

    const typeDefs = [
      `
      type Query {
      
    mma_contests: [MmaContest]
    mma_contest(contest_id:String): MmaContest
	
      }

			type mmaPred {
				key:String
				method : String
				round : Int
				winner_id : String
			}
			input mmaPredInput {
				key:String
				method : String
				round : Int
				winner_id : String
			}
			

	type  mmaPrediction {
		_id:ID
		contest_id: String
		user_id: Int
		user_name:String
		username: String
		league:String
		pool_id:Int
		preds: [mmaPred]
	}

	  type MmaContest {
      _id: ID!
		  name: String
      submitTime: String
		  expireTime: String
		  bouts: [RealBout]
      cashPrize: Int
      entryFee : Int
  		maxPlayers : Int
      messagePlayers : String
  		sendEmailReminders: String
  		invitedPlayers: [String]
		}
		input MmaContestInput {
			id : String
			bouts: [Bout]

		}
		



      type Mutation {
      		createMmaPrediction(contest_id: String,user_id: Int,user_name: String, username:String,league:String,pool_id:Int,preds : [mmaPredInput]): mmaPrediction,
    createMmaContest(name:String, submitTime:String, cashPrize:Int, bouts : [Bout], entryFee : Int, maxPlayers : Int): [MmaContest],

			updateMmaContest (
				input: MmaContestInput,
				): MmaContest,
      }


      schema {
        query: Query
        mutation: Mutation
      }
    `
    ];
    // If Message had any complex fields, we'd put them on this object.

    const resolvers = {
      Query: {
        mma_contests: async () => {
          return (await MmaContests.find({}).toArray()).map(prepare);
        },
        mma_contest: async (root, args) => {
          return await MmaContests.findOne(mongo.ObjectId(args.contest_id));
        }
      },
      Mutation: {
        createFighter: async (root, args, context, info) => {
          const res = await Fighters.insert(args);

          return (await Fighters.find({}).toArray()).map(prepare);
        },
        createMmaContest: async (root, args, context, info) => {
          var errorArray = [];
          args.bouts.forEach(function(bout) {
            bout.key =
              bout["fight_type"] +
              "-" +
              bout["fighters"][0]["_id"] +
              "-" +
              bout["fighters"][1]["_id"];
          });

          const results = await MmaContests.insert(args);
          const contest_id = results.insertedIds[0];
          var theIdToString = contest_id.toString();

          var querySQL =
            "INSERT INTO mma_contest ( `contest_id`,`contest_name`, `max_players`,  `cash_prize`,`entry_fee`, `submit_time`) VALUES ( ?,?,?,?,?,?)";
          var argsArray = [
            theIdToString,
            args.name,
            args.maxPlayers,
            args.cashPrize,
            args.entryFee,
            args.submitTime
          ];
          const createSqlMmaContest = await con.query(querySQL, argsArray);
          var allMmaContests = (await MmaContests.find({}).toArray()).map(
            prepare
          );

          return allMmaContests;
        },

        createMmaPrediction: async (root, args, context, info) => {
          var message = {};
          var res = {};
          //var bracketCheckD = JSON.stringify(bracketCheck)

          //var bracketCheck = UserBrackets.findOne({"contest_id":args.contest_id},{"bracket_id":args.bracket_id},{"user_id":args.user_id})
          delete args["bracket_data"];

          const mmaCheck = await MmaPredictions.find({
            contest_id: args.contest_id,
            pool_id: args.pool_id,
            user_id: args.user_id
          }).toArray();

          if (typeof mmaCheck != "undefined" && mmaCheck.length > 0) {
            try {
              res = await MmaPredictions.findOneAndUpdate(
                {
                  contest_id: args.contest_id,
                  pool_id: args.pool_id,
                  user_id: args.user_id
                },
                {
                  $set: {
                    preds: args.preds,
                    username: args.username,
                    user_name: args.user_name
                  }
                }
              );
            } catch (e) {
              console.log("error is");
              console.log(e);
            }
            res["value"]["success"] = true;
            res["value"]["text"] = "updated";
          } else {
            res = await MmaPredictions.insert(args);

            res["value"] = res.ops[0];
            res["value"]["success"] = true;
            res["value"]["text"] = "created";
          }

          return res["value"];
        },

        updateMmaContest: (_, { input }) => {
          try {
            var mmaContestUpdate = MmaContests.findOneAndUpdate(
              { _id: mongo.ObjectId(input.id) },
              { $set: { bouts: input.bouts } }
            );
          } catch (e) {
            console.log("error is");
            console.log(e);
          }
        }
      }
    };

    const schema = graphqlTools.makeExecutableSchema({
      typeDefs,
      resolvers
    });

    const app = express();

    app.use(cors());

    //adding file upload

    const asyncMiddlewareMmaStandings = async (req, res, next) => {
      var contest_submissions = await MmaPredictions.find({
        contest_id: req.params.contest_id,
        pool_id: parseInt(req.params.pool_id)
      }).toArray();
      var contest = await MmaContests.findOne(
        mongo.ObjectId(req.params.contest_id)
      );

      req.data = {};
      req.data.contest_submissions = contest_submissions;
      req.data.contest = contest;
      next();
    };
    app.get(
      "/build_mma_standings/pool/:pool_id/contest/:contest_id",
      asyncMiddlewareMmaStandings,
      function(req, res) {
        var mmaSubmissions = req.data.contest_submissions;
        var mmaContest = req.data.contest;
        //console.log(req.body)

        var standings = mmaScoreStandingsHold.calculateScores(
          mmaSubmissions,
          mmaContest
        );
        res.send({ standings: standings });
      }
    );
    //this on creates an mma contest_type
    const asyncMiddlewareCreateMmaContest = async (req, res, next) => {
      var nameString = req.body.name;
      var submitTimeString = req.body.submitTime;
      var cashPrizeInt = parseInt(req.body.cashPrize);
      var entryFeeInt = parseInt(req.body.entryFee);
      var maxPlayersInt = parseInt(req.body.maxPlayers);

      var boutInputs = req.body.bouts;

      var query = `
          mutation CreateMmaContestMutation($name:String, $submitTime : String, $bouts:[Bout], $entryFee : Int, $maxPlayers : Int, $cashPrize : Int) {
            createMmaContest(name :$name,submitTime :$submitTime,bouts : $bouts, cashPrize : $cashPrize, maxPlayers : $maxPlayers, entryFee : $entryFee)  {
                 submitTime
                 name
                 bouts {
									 key
									fight_type
									rounds
										fighters {
											_id
											name
										}
								 }

           }

          }
      `;
      const params = {
        name: nameString,
        submitTime: submitTimeString,
        bouts: boutInputs,
        maxPlayers: maxPlayersInt,
        entryFee: entryFeeInt,
        cashPrize: cashPrizeInt
      };

      const mmaReturn = await graphqlClient.graphql(
        schema,
        query,
        null,
        null,
        params
      );

      req.allMmaContests = mmaReturn;

      next();
    };

    app.post("/create-mma-contest", asyncMiddlewareCreateMmaContest, function(
      req,
      res
    ) {
      console.log("req after");
      console.log(req.allMmaContests.data.createMmaContest);
      res.send({ MmaContests: req.allMmaContests.data.createMmaContest });
    });

    //this on creates an mma contest_type
    const asyncMiddlewareSubmitMmaPredictions = async (req, res, next) => {
      console.log("req");
      console.log(req.body);
      var contestIdString = req.body.contest_id;
      var user_nameString = req.body.user_name;
      var userNameString = req.body.username;
      var leagueString = req.body.sport;
      var userId = parseInt(req.body.user_id);
      var poolId = parseInt(req.body.pool_id);

      var predInputs = req.body.preds;

      var query = `
			mutation CreateMmaPredictionMutation($contest_id:String, $user_id : Int, $user_name:String, $username:String,$preds:[mmaPredInput], $pool_id : Int, $league : String) {
				createMmaPrediction(contest_id: $contest_id,user_id: $user_id,user_name: $user_name, username:$username,league:$league,pool_id:$pool_id,preds : $preds){
					contest_id
						 user_id
						 username
						 user_name
						 pool_id
						 league
						 preds {
							 key
							round
							winner_id
							method
						 }

			 }

			}
	`;
      const params = {
        contest_id: contestIdString,
        user_id: userId,
        user_name: user_nameString,
        username: userNameString,
        league: leagueString,
        pool_id: poolId,
        preds: predInputs
      };
      console.log("params");
      console.log(params);

      const mmaReturn = await graphqlClient.graphql(
        schema,
        query,
        null,
        null,
        params
      );
      console.log("mmaReturn is");
      console.log(mmaReturn);

      //with args for strings check for undefined and make sure the array has at least one value or else throw an error

      //})
      req.mmaContestResults = mmaReturn;

      next();
    };

    app.post(
      "/submit-mma-predictions",
      asyncMiddlewareSubmitMmaPredictions,
      function(req, res) {
        console.log("req after");
        console.log(req.mmaContestResults);
        res.send({
          MmaContestsResult: req.mmaContestResults.data.createMmaPrediction
        });
      }
    );

    //retrieve an mma contest_type
    const asyncMiddlewareMma = async (req, res, next) => {
      var use_this_id =
        req.params.other_user_id > 0
          ? req.params.other_user_id
          : req.params.user_id;
      var contest = await MmaContests.findOne(
        mongo.ObjectId(req.params.contest_id)
      );
      const mmaPredictions = await MmaPredictions.find({
        contest_id: req.params.contest_id,
        pool_id: parseInt(req.params.pool_id),
        user_id: parseInt(use_this_id)
      }).toArray();

      req.data = {};
      req.data.contest = contest;
      req.data.results = mmaPredictions;

      next();
    };

    app.get(
      "/build_fights/pool/:pool_id/contest/:contest_id/user/:user_id/other_user/:other_user_id",
      asyncMiddlewareMma,
      (req, res) => {
        const boutsByKey = {};
        req.data.contest.bouts.forEach(function(bout) {
          boutsByKey[
            bout.fight_type +
              "-" +
              bout.fighters[0]["_id"] +
              "-" +
              bout.fighters[1]["_id"]
          ] = bout;
        });

        var contestScore = 0;
        if (req.data.results.length > 0) {
          contestScore = mmaScoreStandingsHold.getScore(
            req.data.results[0]["preds"],
            boutsByKey
          );
        }
        res.send({
          mma_contest: req.data.contest,
          user_picks: req.data.results,
          contest_score: contestScore
          //});
        });
        //return matches;
      }
    );

    var server = https.createServer(options, app);
    server.listen(PORT, () => {
      console.log(
        `GraphiQL is now running on http://localhost:${PORT}/graphiql. The home directory is ${process.cwd()}`
      );
    });
  } catch (e) {}
};
