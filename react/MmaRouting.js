import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Mma from './Mma'
import Fighters from './Fighters'
import MmaContests from './MmaContests'
import MmaContestsResults from './MmaContestsResults'

// The Roster component matches one of two different routes
// depending on the full pathname
const MmaRouting = () => (
  <Switch>
    <Route exact path='/mma' component={Mma}/>
    <Route path='/mma/fighters' component={Fighters}/>
    <Route path='/mma/create-contests' component={MmaContests}/>
    <Route path='/mma/enter-results/:contest_id' component={MmaContestsResults}/>
  </Switch>
)

export default MmaRouting
