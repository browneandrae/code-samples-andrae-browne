import axios from "axios";

import React from "react";

import dbAPI from "../apis/dbAPI";

import { Link } from "react-router-dom";

const MmaContests = React.createClass({
  getInitialState: function() {
    return {
      MMACONTESTS: [],
      FIGHTERS: [],
      bouts: [{}]
    };
  },

  //Stuff from demo

  handleAddMmaContest: function() {
    this.setState({ bouts: this.state.bouts.concat([{}]) });
  },

  handleRemoveMmaContest: function(idx) {
    if (idx > 0)
      this.setState({
        bouts: this.state.bouts.filter((s, sidx) => idx !== sidx)
      });
  },
  handleNoPlayers: function(e) {
    this.setState({ maxPlayers: e.target.value });
  },
  handleEntryFee(e) {
    this.setState({ entryFee: e.target.value });
  },

  handleCashPrize(e) {
    this.setState({ cashPrize: e.target.value });
  },
  handleDelete: function(event) {
    dbAPI.deleteObject(event);
    setTimeout(() => {
      this.loadMmaContests();
    }, 1000);
  },
  getABout: function(idx) {
    return this.addmmaContest(idx);
  },

  //End stuff from demo

  handleFightType: function(idx, e) {
    if (typeof this.state.bouts[idx] == "undefined") {
      this.state.bouts[idx] = {};
    }
    this.state.bouts[idx]["fight_type"] = e.target.value;
    if (
      typeof this.state.bouts[idx]["fight_type"] != "undefined" &&
      typeof this.state.bouts[idx]["fighters"][0]["_id"] != "undefined" &&
      typeof this.state.bouts[idx]["fighters"][1]["_id"] != "undefined"
    ) {
      this.state.bouts[idx]["key"] =
        this.state.bouts[idx]["fight_type"] +
        "-" +
        this.state.bouts[idx]["fighters"][0]["_id"] +
        "-" +
        this.state.bouts[idx]["fighters"][1]["_id"];
    }

    this.setState({ bouts: this.state.bouts });
  },

  handleFighter: function(indices, e) {
    if (typeof this.state.bouts[indices["idx"]] == "undefined") {
      this.state.bouts[indices["idx"]] = {};
    }

    if (typeof this.state.bouts[indices["idx"]]["fighters"] == "undefined") {
      this.state.bouts[indices["idx"]]["fighters"] = [];
      this.state.bouts[indices["idx"]]["fighters"][0] = {};
      this.state.bouts[indices["idx"]]["fighters"][1] = {};
    }
    var index = e.target.selectedIndex;
    var optionElement = e.target.childNodes[index];
    var fighterName = optionElement.getAttribute("data-name");

    this.state.bouts[indices["idx"]]["fighters"][indices["fighterIndex"]][
      "_id"
    ] = e.target.value;
    this.state.bouts[indices["idx"]]["fighters"][indices["fighterIndex"]][
      "name"
    ] = fighterName;
    if (
      typeof this.state.bouts[indices["idx"]]["fight_type"] != "undefined" &&
      typeof this.state.bouts[indices["idx"]]["fighters"][0]["_id"] !=
        "undefined" &&
      typeof this.state.bouts[indices["idx"]]["fighters"][1]["_id"] !=
        "undefined"
    ) {
      this.state.bouts[indices["idx"]]["key"] =
        this.state.bouts[indices["idx"]]["fight_type"] +
        "-" +
        this.state.bouts[indices["idx"]]["fighters"][0]["_id"] +
        "-" +
        this.state.bouts[indices["idx"]]["fighters"][1]["_id"];
    }
    this.setState({ bouts: this.state.bouts });
  },
  handleRounds: function(idx, e) {
    if (typeof this.state.bouts[idx] == "undefined") {
      this.state.bouts[idx] = {};
    }

    this.state.bouts[idx]["rounds"] = e.target.value;
    this.setState({ bouts: this.state.bouts });
  },

  handleSubmitDateStart(e) {
    this.setState({ submitstart: e.target.value });
  },
  handleSubmitTimeStart(e) {
    this.setState({ contestsubmittime: e.target.value });
  },
  handleNameChange: function(e) {
    this.setState({ contestName: e.target.value });
  },
  getFighterList(indices) {
    return (
      <select
        name="fighter_options"
        onChange={this.handleFighter.bind(this, indices)}
        className="form-control"
      >
        {this.state.FIGHTERS.map(p => (
          <option data-name={p.name} value={p._id}>
            {p.name}
          </option>
        ))}
      </select>
    );
  },
  addmmaContest: function(idx) {
    return (
      <div>
        <div class="row">
          <div class="col-sm-12">
            <h2>New Bout</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <label className="control-label">Fight Type</label>
            <select
              onChange={this.handleFightType.bind(this, idx)}
              className="form-control"
            >
              <option value="main-event">Main Event</option>
              <option value="co-main-event">Co-Main Event</option>
              <option value="main-card">Main Card</option>
              <option value="prelim-fight">Prelim Fight</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <label className="control-label">Fighter 1</label>
            {this.getFighterList({ idx, fighterIndex: 0 })}
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <label className="control-label">Fighter 2</label>
            {this.getFighterList({ idx, fighterIndex: 1 })}
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <label className="control-label">Number of Rounds</label>
            <select
              onChange={this.handleRounds.bind(this, idx)}
              className="form-control"
            >
              <option value="0">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
          </div>
        </div>
      </div>
    );
  },

  submitMmaContest: function(event) {
    //let's make the timeout
    if (typeof this.state.submitstart == "undefined") {
      alert("Please set the submit date");
    }
    if (typeof this.state.contestsubmittime == "undefined") {
      alert("Please set the submit time");
    }
    if (
      typeof this.state.contestsubmittime == "undefined" ||
      typeof this.state.submitstart == "undefined"
    ) {
      return false;
    }

    event.preventDefault();

    event.stopPropagation();

    var submitTimeDead =
      this.state.submitstart + " " + this.state.contestsubmittime;
    var postObject = {
      bouts: this.state.bouts,
      submitTime: submitTimeDead,
      name: this.state.contestName,
      cashPrize: this.state.cashPrize,
      entryFee: this.state.entryFee,
      maxPlayers: this.state.maxPlayers
    };

    axios.post(globalApiUrl + "create-mma-contest", postObject).then(res => {
      var mmaContests = res.data.MmaContests;

      this.setState({
        MMACONTESTS: mmaContests
      });
    });
  },
  loadMmaContests: function() {
    axios
      .get(
        `${globalApiUrl}graphql?query=query{mma_contests{name _id submitTime }}`
      )

      .then(res => {
        var mmaContests = res.data.data.mma_contests;

        this.setState({
          MMACONTESTS: mmaContests
        });
      });
  },

  loadFighters: function() {
    axios
      .get(`${globalApiAdminUrl}/graphql?query=query{fighters{name _id }}`)

      .then(res => {
        var fighters = res.data.data.fighters;

        this.setState({
          FIGHTERS: fighters
        });
      });
  },

  componentDidMount: function() {
    this.loadMmaContests();
    this.loadFighters();
  },

  render: function() {
    const { mmaContestName } = this.state;

    return (
      <div>
        <h1>Add MmaContest</h1>
        <div
          id="create-mmaContest-form"
          className="addmmaContestForm"
          onSubmit={this.submitMmaContest}
          method="POST"
          action=""
        >
          <div class="top-mmap-wrap">
            <div class="form-control">
              <label className="control-label">Contest Name</label>
              <input
                type="text"
                onChange={this.handleNameChange}
                name="new-mmaContest"
                className="form-control"
                placeholder="Contest Name"
              />
            </div>
          </div>

          <div class="form-control">
            <label className="control-label">
              Deadline to Submit Mma Contest?
            </label>
            <input
              className="form-control"
              id="mma-submit-date"
              onChange={this.handleSubmitDateStart}
              type="date"
            />
            <label className="control-label">Time?</label>
            <input
              id="mma-submit-time"
              name="mma-submit-time"
              onChange={this.handleSubmitTimeStart}
              type="time"
            />
          </div>
          <div class="form-control">
            <label className="control-label">Max Players</label>

            <input
              className="form-control"
              type="number"
              onChange={this.handleNoPlayers}
            />
          </div>
          <div className="form-control">
            <label className="control-label">Entry Fee</label>
            <input
              type="number"
              placeholder="Entry Fee"
              onChange={this.handleEntryFee}
              min="0"
            />
          </div>
          <div className="form-control">
            <label className="control-label">Cash Prize</label>
            <input
              type="number"
              placeholder="Cash Prize"
              onChange={this.handleCashPrize}
              min="0"
            />
          </div>
          {this.state.bouts.map((map, idx) => (
            <div>
              {this.getABout(idx)}
              <button type="button" onClick={this.handleAddMmaContest}>
                Add Bout
              </button>
              <button
                type="button"
                onClick={() => this.handleRemoveMmaContest(idx)}
              >
                Remove Bout
              </button>
            </div>
          ))}

          <input type="submit" value="Submit" onClick={this.submitMmaContest} />
        </div>
        <h1>All MmaContests</h1>
        <ul>
          {this.state.MMACONTESTS.map(mmaContest => (
            <li>
              <Link to={"/mma/enter-results/" + mmaContest._id}>
                {mmaContest.name} {mmaContest.submitTime}
                <input
                  style={{ marginLeft: "30px" }}
                  className="delete-button"
                  type="button"
                  data-type="mmaContest"
                  data-id={mmaContest._id}
                  onClick={event => this.handleDelete(event)}
                  value="Delete MmaContest"
                />
              </Link>
            </li>
          ))}
        </ul>
      </div>
    );
  }
});

export default MmaContests;
