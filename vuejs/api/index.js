import Vue from "vue";
import Utils from "../helpers/Utils";
/*
export default
    axios.create({
        baseURL: 'http://mediconnect.bluenimble.space:9090/playground/api'
    });
	*/
const baseURL = process.env.VUE_APP_BASE_URL;
const corsProxy = "https://cors-anywhere.herokuapp.com/";
console.log("baseURL");
console.log(baseURL);
export const apiModel = {
  Authentication: {
    Login: {
      method: "POST",
      url: "/security/login"
    },
    SignUp: {
      method: "POST",
      url: "/security/signup"
    }
  },
  Profile: {
    CreateSections: {
      method: "POST",
      url: " /profile/sections/:section/entries"
    },
    UpdateSections: {
      method: "PUT",
      url: " /profile/sections/:section/entries/:entry"
    }
  },
  Post: {
    Create: {
      method: "POST",
      url: "/posts"
    },
    List: {
      method: "GET",
      url: " /posts/:category/list?start=:start&count=:count"
    },
    AddComment: {
      method: "POST",
      url: "/comments/post/:postId"
    }
  },
  Comments: {
    GetThem: {
      method: "GET",
      url: "/comments/:target/:id?start=:start&count=:count"
    }
  }
};
function errorLife(error) {
  console.log("error");
  console.log(error);
  if (typeof error.body != "") {
    if (typeof error.body.message != "undefined") {
      Vue.notify({
        group: "loggedIn",
        type: "error",
        text: error.body.message
      });
    } else {
      var payloadWrap = error.body.message.payload;
      console.log(error.body.message.payload);

      Object.keys(payloadWrap).forEach(function(key) {
        if (payloadWrap[key].hasOwnProperty("spec")) {
          console.log("has this");
          console.log(key);
          if (payloadWrap[key]["spec"].hasOwnProperty("errMsg")) {
            console.log("has all of this");
            Vue.notify({
              group: "loggedIn",
              type: "error",
              text: payloadWrap[key]["spec"]["errMsg"]
            });
          }
        }
      });
    }
  } else {
    Vue.notify({
      group: "loggedIn",
      type: "error",
      text: "Something bad happened"
    });
  }
}
export async function apiUrlAndGo(
  args,
  object,
  token = "",
  successCallback = {},
  errorCallback = {}
) {
  //error callback not being used
  console.log("we in here");

  var methodVar = object.method;
  var urlWithPlaceholders = object.url;

  var urlNoPlace = "";

  Object.keys(args).forEach(function(key) {
    var placeholder = key;

    urlNoPlace = urlWithPlaceholders.replace(`:${placeholder}`, args[key]);
    urlWithPlaceholders = urlNoPlace;
  });

  if (args.hasOwnProperty("section")) {
    delete args["section"];
  }
  if (args.hasOwnProperty("entry")) {
    delete args["entry"];
  }
  if (args.hasOwnProperty("postId")) {
    delete args["postId"];
  }

  var customHeaders = {};

  customHeaders["headers"] = {};

  if (token != "") {
    customHeaders["headers"]["Authorization"] = Utils.auth();
  }
  let response;

  if (methodVar == "POST") {
    console.log("posting");

    try {
      response = await Vue.http
        .post(baseURL + urlNoPlace.trim(), args, customHeaders)
        .then(successCallback, error => {
          errorLife(error);
          // error callback
        });
    } catch (ex) {
      // Handle error
      return;
    }
  } else if (methodVar == "GET") {
    try {
      response = await Vue.http
        .get(baseURL + urlNoPlace.trim(), customHeaders)
        .then(successCallback, error => {
          errorLife(error);
          // error callback
        });
    } catch (ex) {
      // Handle error
      return;
    }
  } else if (methodVar == "PUT") {
    console.log("putting");

    try {
      response = await Vue.http
        .put(baseURL + urlNoPlace.trim(), args, customHeaders)
        .then(successCallback, error => {
          errorLife(error);
          // error callback
        });
    } catch (ex) {
      // Handle error
      return;
    }
  }
}
