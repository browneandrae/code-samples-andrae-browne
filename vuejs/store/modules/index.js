/**
 * Auth Module
 */
import Vue from "vue";
import Nprogress from "nprogress";
import router from "../../../router";
import { apiModel, apiUrlAndGo } from "../../../api";
const state = {
  user: JSON.parse(localStorage.getItem("user")),
  isUserSigninWithAuth0: Boolean(localStorage.getItem("isUserSigninWithAuth0"))
};

// getters
const getters = {
  getUser: state => {
    return state.user;
  },
  isUserSigninWithAuth0: state => {
    return state.isUserSigninWithAuth0;
  }
};

// actions
const actions = {
  logoutUserFromBlueNimble(context) {
    context.commit("signOutUserFromAuth0Success");
  },
  signinUserInBlueNimble(context, payload) {
    context.commit("signInUserWithAuth0Success", payload.user);
  },
  signupUserInBlueNimble(context, payload) {
    context.commit("signUpUserWithAuth0Success", payload.userDetail);
  },
  editProfileHandlerAuth(context, payload) {
    context.commit("editProfileHandlerAuth", payload);
  }
};

// mutations
const mutations = {
  loginUser(state) {
    Nprogress.start();
  },
  editProfileHandlerAuth: function(state, payload) {
    try {
      const oneItem =
        payload.user.sections[payload.slug]["entries"][payload.entryKey];

      var entries = {};
      entries[payload.entryKey] = oneItem;
      const slug = payload.slug;

      const localToken = localStorage.getItem("mc-token");

      Object.keys(entries).forEach(function(entryKey) {
        if (
          oneItem.hasOwnProperty("permanentKey") &&
          typeof oneItem["borned"] == "undefined"
        ) {
          entryKey = oneItem["permanentKey"];
        }

        var args = entries[entryKey];

        args["section"] = slug;

        function successCallback(response) {
          // two steps
          const responseKey = response.data.entry;
          if (
            payload.user.sections[slug]["entries"][entryKey].hasOwnProperty(
              "creating"
            )
          ) {
            if (
              payload.user.sections[payload.slug]["entries"][entryKey][
                "creating"
              ] == true
            ) {
              /*
						console.log("deleting this key")
					console.log(entryKey)
				
					console.log("entries[entryKey] is now")
					console.log(JSON.stringify(entries))
					console.log("response.data.entry")
					console.log(response.data.entry)
					*/
              state.user["sections"][slug]["entries"][responseKey] =
                entries[entryKey];
              state.user["sections"][slug]["entries"][responseKey][
                "creating"
              ] = false;
              state.user["sections"][slug]["entries"][responseKey][
                "edited"
              ] = false;
              Vue.set(
                state.user["sections"][slug]["entries"][responseKey],
                "id",
                responseKey
              );

              //console.log("just deleting")

              Vue.delete(state.user.sections[slug]["entries"], entryKey);
            }
          } else if (
            payload.user.sections[payload.slug]["entries"][
              entryKey
            ].hasOwnProperty("edited")
          ) {
            if (
              payload.user.sections[payload.slug]["entries"][entryKey][
                "edited"
              ] == true
            ) {
              Vue.set(
                state.user["sections"][slug]["entries"],
                entryKey,
                entries[entryKey]
              );
              state.user["sections"][slug]["entries"][entryKey][
                "edited"
              ] = false;
            }
          }

          localStorage.setItem("user", JSON.stringify(state.user));

          if (response.data.hasOwnProperty("entry")) {
            Vue.notify({
              group: "loggedIn",
              type: "success",
              text: "User Profile Entry Created"
            });
          } else if (response.data.hasOwnProperty("updated")) {
            Vue.notify({
              group: "loggedIn",
              type: "success",
              text: "User Profile Entry Updated"
            });
          }
        }
        function errorCallback(error) {}

        if (typeof entries[entryKey]["borned"] == "undefined") {
          args["borned"] = true;

          apiUrlAndGo(
            args,
            apiModel.Profile.CreateSections,
            localToken,
            successCallback,
            errorCallback
          );
        } else {
          //	console.log("trying an update")
          args["entry"] = entryKey;
          apiUrlAndGo(
            args,
            apiModel.Profile.UpdateSections,
            localToken,
            successCallback,
            errorCallback
          );
        }
      });
    } catch (e) {
      console.log("error is");
      console.log(e);
    }

    //api push for the user would go here since the state contains user updated description for each section i believe let me check
    //state.user = true;
  },
  loginUserSuccess(state, user) {
    state.user = user;
    localStorage.setItem("user", JSON.stringify(user));
    localStorage.setItem("mc-token", user.token);
    state.isUserSigninWithAuth0 = false;
    router.push("/default/dashboard/ecommerce");
    setTimeout(function() {
      Vue.notify({
        group: "loggedIn",
        type: "success",
        text: "User Logged In Success!"
      });
    }, 1500);
  },
  loginUserFailure(state, error) {
    Nprogress.done();
    Vue.notify({
      group: "loggedIn",
      type: "error",
      text: error.message
    });
  },

  signUpUser(state) {
    Nprogress.start();
  },
  signUpUserSuccess(state, user) {
    state.user = localStorage.setItem("user", user);
    localStorage.setItem("mc-token", user.token);
    router.push("/feed");
    Vue.notify({
      group: "loggedIn",
      type: "success",
      text: "Account Created!"
    });
  },
  signUpUserFailure(state, error) {
    Nprogress.done();
    Vue.notify({
      group: "loggedIn",
      type: "error",
      text: error.message
    });
  },
  signInUserWithAuth0Success(state, user) {
    //login
    function successCallback(response) {
      //Vue.set(state, user, response.data)
      state.user = response.data;
      localStorage.setItem("user", JSON.stringify(response.data));
      localStorage.setItem("mc-token", response.data.token);
      router.push("/feed");
      Vue.notify({
        group: "loggedIn",
        type: "success",
        text: "Signed In"
      });
    }
    function errorCallback(error) {
      if (typeof error.body.message != "undefined") {
        Vue.notify({
          group: "loggedIn",
          type: "error",
          text: error.body.message
        });
      } else {
        var payloadWrap = error.body.message.payload;
        Object.keys(payloadWrap).forEach(function(key) {
          if (payloadWrap[key].hasOwnProperty("spec")) {
            /* console.log("has this")
					 console.log(key) */
            if (payloadWrap[key]["spec"].hasOwnProperty("errMsg")) {
              //console.log("has all of this")
              Vue.notify({
                group: "loggedIn",
                type: "error",
                text: payloadWrap[key]["spec"]["errMsg"]
              });
            }
          }
        });
      }
    }

    apiUrlAndGo(
      user,
      apiModel.Authentication.Login,
      "",
      successCallback,
      errorCallback
    );
  },
  signUpUserWithAuth0Success(state, user) {
    //register
    function successCallback(response) {
      localStorage.setItem("user", JSON.stringify(response.data));
      localStorage.setItem("mc-token", response.data.token);
      state.user = response.data;

      router.push("/feed");
      Vue.notify({
        group: "loggedIn",
        type: "success",
        text: "Account Created!"
      });
    }
    function errorCallback(error) {
      if (typeof error.body.message != "undefined") {
        Vue.notify({
          group: "loggedIn",
          type: "error",
          text: error.body.message
        });
      } else {
        var payloadWrap = error.body.message.payload;
        Object.keys(payloadWrap).forEach(function(key) {
          if (payloadWrap[key].hasOwnProperty("spec")) {
            if (payloadWrap[key]["spec"].hasOwnProperty("errMsg")) {
              /*console.log("has all of this") */
              Vue.notify({
                group: "loggedIn",
                type: "error",
                text: payloadWrap[key]["spec"]["errMsg"]
              });
            }
          }
        });
      }
    }

    apiUrlAndGo(
      user,
      apiModel.Authentication.SignUp,
      "",
      successCallback,
      errorCallback
    );
  },

  signOutUserFromAuth0Success(state) {
    state.user = null;
    localStorage.removeItem("user");
    router.push("/session/login");
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
